exports.success = function (data) {
    return { success: true, data: data };
};

exports.error = function (error) {
    console.log('error = ' + error)
    return { success: false, error:  error };
};