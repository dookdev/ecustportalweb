module.exports = {
    apps: [
        {
            name: "myapp",
            script: "./app.js",
            watch: false,
            instances: "max",
            exec_mode: "cluster",
            env: {
                "NODE_ENV": "development"
            },
            env_staging: {
                "NODE_ENV": "staging",
            },
            env_production: {
                "NODE_ENV": "production",
            }
        }
    ]
}