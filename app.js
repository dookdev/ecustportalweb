const express = require('express');
const https = require('https');
const path = require('path');
const fs = require('fs');
const bodyParser = require('body-parser');
const cors = require('cors');
const request = require('request');
const conf = require('./node-env').config();

const app = express();
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
    next();
});
app.use(cors({ origin: '*' }));
app.use(bodyParser.urlencoded({ extended: false, limit: '20mb' }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'dist')));
app.use(express.static(path.join(__dirname, 'dist/eCustPortal')));
app.post('/api/callBackend', (req, res) => {
    request(
        {
            method: req.body.method ? req.body.method : 'POST',
            rejectUnauthorized: false,
            strictSSL: false,
            json: true,
            headers: { 'Content-Type': 'application/json' },
            url: (conf.url ? conf.url : 'https://localhost:3000/api/') + (req.body.service ? req.body.service : ''),
            form: req.body.data ? req.body.data : {}
        },
        function (error, response, body) {
            if (error) {
                res.status(404).json(error);
            } else {
                res.json(body);
            }
        });
});

app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/eCustPortal/index.html'));
});

const port = conf.port;
var options = {
    key: fs.readFileSync(__dirname + '/security/cert.key'),
    cert: fs.readFileSync(__dirname + '/security/cert.pem'),
    strictSSL: false,
    requestCert: false,
    rejectUnauthorized: false
};

app.set('port', port);
const server = https.createServer(options, app);
server.listen(port, () => console.log('Web Running at port: ' + port));
server.setTimeout(1800000);